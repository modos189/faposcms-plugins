<?php

$output    = '';
$template_patch = dirname(__FILE__).'/template/index.html';
$conf_pach = dirname(__FILE__).'/config.json';
$config = json_decode(file_get_contents($conf_pach), true);

if (isset($_POST['send'])) {
    $config['limit'] = $_POST['limit'];
    $config['only_active_module'] = (!empty($_POST['only_active_module'])) ? 1 : 0;
    file_put_contents($conf_pach, json_encode($config));

    $f = fopen($template_patch, "w");
    fwrite($f,$_POST['template']);

    $output .= '<div class="warning">Сохранено!<br><br></div>';
}

$template = file_get_contents($template_patch);

$l = ($config['only_active_module']) ? 'checked="checked" ' : '';
$output .= '<style>
                .ib {font-weight: bold; font-style: italic;}
                .right {width: 50%;}
                input[type="text"] {height: auto}
                .markers input {
                    background: #D9D9D9;
                    border-radius: 5px;
                    display: inline-block;
                    font-weight: 700;
                    padding: 5px;border:none;width:130px;text-align:center;
                }
            </style>';

$output .= '<form action="" method="post">
                <div class="list">
                    <div class="title">Управление плагином вывода похожих материалов</div>
                    <div class="level1">

                            <div class="items">
                                <div class="setting-item">
                                    <div class="title" style="padding: 20px; text-align:center;">Вывести виджет на страницу можно с помощью метки {{ relmat }}</div>
                                </div>
                            </div>

                            <div class="items">
                                <div class="setting-item">
                                    <div class="left">Максимальное количество рекомендаций:</div>
                                    <div class="right">
                                        <input type="text" size="100" name="limit" value="' . $config['limit'] . '">
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>

                            <div class="items">
                                <div class="setting-item">
                                    <div class="left">Рекомендации только из активного модуля:<br><small>Например, при просмотре новости в рекомендациях будут только новости</small></div>
                                    <div class="right">
                                        <input type="checkbox" name="only_active_module" id="52f1872a9c47b688089e8816ba9b1d9d" '.$l.'value="1">
                                        <label for="52f1872a9c47b688089e8816ba9b1d9d"> </label>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>

                            <div class="items">
                                <div class="setting-item">
                                    <textarea name="template" style="width: 99%; height: 350px">' . (isset($template) ? htmlspecialchars($template) : '') . '</textarea>
                                </div>
                            </div>

                            <div class="items">
                                <div class="setting-item">
                                    <div class="title" style="padding: 20px; text-align:center; height: auto;"><input name="send" type="submit" value="Записать" class="save-button"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>';

$output .= '<ul class="markers">
                <li><input onclick="select(this)" value="{{ materials }}" readonly="" /> - массив с материалами. С помощью {% for material in materials %} можно выводить материалы.</li>
                <li>Для вывода доступны все поля, записанные в БД для материала. Вот некоторые из них:</li>
                <li><input onclick="select(this)" value="{{ material.id }}" readonly="" /> - ID выводимого материала.</li>
                <li><input onclick="select(this)" value="{{ material.title }}" readonly="" /> - Заголовок.</li>
                <li><input onclick="select(this)" value="{{ material.views }}" readonly="" /> - Количество просмотров.</li>
                <li><input onclick="select(this)" value="{{ material.data }}" readonly="" /> - Дата добавления.</li>
                <li>Также доступны дополнительные метки:</li>
                <li><input onclick="select(this)" value="{{ material.add_markers.url }}" readonly="" /> - URL адрес материала.</li>
                <li><input onclick="select(this)" value="{{ material.add_markers.module }}" readonly="" /> - Модуль материала.</li>
                <li><input onclick="select(this)" value="{{ material.add_markers.img }}" readonly="" /> - URL адрес первой прикреплённой картинки, если есть.</li>
            </ul>';

?>