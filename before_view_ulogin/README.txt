
	Плагин uLogin для Fapos CMS 2.x

Плагин uLogin позволяет входить на сайт через социальные сети, без ручной регистрации на сайте.
Виджет с иконками сервисов выводится меткой {{ ulogin }}

Что тут делается:
 - Автоматическая регистрация на сайте используя данные из выбранного сервиса
 - Если ник занят или не соответствует требованиям то появляется промежуточная страница со сменой ника
 - После регистрации редирект на свою страницу
 - Если просто зашел то редирект на главную
 - После регистрации приходит личное сообщение с приветствием и паролем
 - В админке в настройке плагина можно сменить отображаемые сервисы в виджете и обязательные поля

Установка:
В форке от modos189 https://github.com/modos189/Fapos2.x/ для установки плагина достаточно выбрать его в каталоге плагинов и нажать на кнопку Установить.

В устаревших версиях или других форках необходимо выполнить следующие действия:

1. Распаковать архив и переместить папку before_view_ulogin в sys/plugins.
2. Для файла sys/plugins/before_view_ulogin/config.php выставить права 777.
3. Выполнить запросы к БД, указаны ниже.
4. Добавить метку {{ ulogin }} в нужном месте шаблона.

Запросы к БД:

ALTER TABLE `users` ADD `provider` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ;

ALTER TABLE `users` ADD `full_name` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL AFTER `name` ;

CREATE TABLE `users_ulogin` (
    `id` INT( 11 ) NOT NULL AUTO_INCREMENT ,
	`identity` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
	`nickname` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
	PRIMARY KEY ( `id` )
)