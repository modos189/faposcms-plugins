<?php

function config_write($set) {
	if ($fopen=@fopen(ROOT . '/sys/plugins/before_view_minecraft_skins/config.php', 'w')) {
		$data = '<?php ' . "\n" . '$conf = ' . var_export($set, true) . "\n" . '?>';
		fputs($fopen, $data);
		fclose($fopen);
	}
}

if (isset($_POST['send'])) {
	$TempSet['dir_skins'] = $_POST['dir_skins'];
	$TempSet['dir_cloaks'] = $_POST['dir_cloaks'];
	config_write($TempSet);
}

include ('config.php');

$output = '';

$output .= '<table class="settings-tb">';
$output .= '<form action="" method="post">';

$output .= '<tr>';
$output .= '	<td class="left">Путь к папке со скинами (со слешем в конце):<br>';
$output .= '        <small>к примеру <b>skins/</b> укажет путь к <b>http://сайт/skins/</b></small></td>';
$output .= '	<td>';
$output .= '			<input type="text" size="100" name="dir_skins" value="' . $conf['dir_skins'] . '">';
$output .= '	</td>';
$output .= '</tr>';

$output .= '<tr>';
$output .= '	<td class="left">Путь к папке с плащами (со слешем в конце):<br>';
$output .= '        <small>к примеру <b>cloak/</b> укажет путь к <b>http://сайт/cloak/</b></small></td>';
$output .= '	<td>';
$output .= '		<input type="text" size="100" name="dir_cloaks" value="' . $conf['dir_cloaks'] . '">';
$output .= '	</td>';
$output .= '</tr>';

$output .= '<tr>';
$output .= '	<td colspan="2" align="center">';
$output .= '		<input name="send" type="submit" value="Записать">';
$output .= '	</td>';
$output .= '</tr>';

$output .= '</form>';
$output .= '</table>';

?>